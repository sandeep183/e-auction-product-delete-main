package com.eauction.seller.deleteproduct.service;

import com.eauction.seller.deleteproduct.dto.ProductDeleteInputDTO;
import com.eauction.seller.deleteproduct.entity.BuyerBidDetail;
import com.eauction.seller.deleteproduct.entity.ProductDetail;
import com.eauction.seller.deleteproduct.exception.BidEndedException;
import com.eauction.seller.deleteproduct.exception.BidExistsException;
import com.eauction.seller.deleteproduct.repository.BidDetailsRepository;
import com.eauction.seller.deleteproduct.repository.ProductDetailsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DeleteProductServiceTest {
    @Mock
    private ProductDetailsRepository productDetailsRepository;
    @Mock
    private BidDetailsRepository bidDetailsRepository;
    @Mock
    private SnsClient snsClient;
    @InjectMocks
    private DeleteProductService deleteProductService;

    @Test
    void testDeleteProductDetails() {
        ProductDeleteInputDTO productDeleteInputDTO = getProductDeleteInputDTO();
        Mockito.when(bidDetailsRepository.fetchBids(productDeleteInputDTO.getProductId())).thenReturn(null);
        Mockito.when(productDetailsRepository.fetchProductById(productDeleteInputDTO.getProductId())).thenReturn(getProductDetail());
     //   Mockito.when(snsClient.publish(Mockito.any(PublishRequest.class))).thenReturn(PublishResponse.builder().build());
        Mockito.when(productDetailsRepository.deleteProduct(productDeleteInputDTO.getProductId())).thenReturn(getProductDetail());
        ProductDetail productDetail = deleteProductService.deleteProductDetails(productDeleteInputDTO);
        assertEquals("1111-2222", productDetail.getProductId());
    }

    @Test
    void testBidEndedException() {
        ProductDeleteInputDTO productDeleteInputDTO = getProductDeleteInputDTO();
        Mockito.when(bidDetailsRepository.fetchBids(productDeleteInputDTO.getProductId())).thenReturn(null);
        Mockito.when(productDetailsRepository.fetchProductById(productDeleteInputDTO.getProductId())).thenReturn(getProductDetailEndedBid());
        Exception exception = assertThrows(BidEndedException.class, () -> deleteProductService.deleteProductDetails(productDeleteInputDTO));
        assertEquals("com.eauction.seller.deleteproduct.exception.BidEndedException",
                exception.getClass().getName());
    }

    @Test
    void testBidExistsException() {
        ProductDeleteInputDTO productDeleteInputDTO = getProductDeleteInputDTO();
        Mockito.when(bidDetailsRepository.fetchBids(productDeleteInputDTO.getProductId())).thenReturn(getBidDetails());
        Exception exception = assertThrows(BidExistsException.class, () -> deleteProductService.deleteProductDetails(productDeleteInputDTO));
        assertEquals("com.eauction.seller.deleteproduct.exception.BidExistsException",
                exception.getClass().getName());
    }

    @Test
    void testNullProduct() {
        ProductDetail productDetail = deleteProductService.deleteProductDetails(null);
        assertNull(productDetail.getProductId());
    }

    private ProductDeleteInputDTO getProductDeleteInputDTO() {
        ProductDeleteInputDTO productDeleteInputDTO = new ProductDeleteInputDTO();
        productDeleteInputDTO.setProductId("1111-2222");
        return productDeleteInputDTO;
    }

    private ProductDetail getProductDetail() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setProductId("1111-2222");
        productDetail.setBidEndDate("12/12/2023");
        return productDetail;
    }

    private ProductDetail getProductDetailEndedBid() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setProductId("1111-2222");
        productDetail.setBidEndDate("12/12/2021");
        return productDetail;
    }

    private List<BuyerBidDetail> getBidDetails() {
        List<BuyerBidDetail> buyerBidDetails = new ArrayList<>();
        BuyerBidDetail buyerBidDetail = new BuyerBidDetail();
        buyerBidDetail.setProductId("1111-2222");
        buyerBidDetail.setBidAmount("1000");
        buyerBidDetail.setEmail("abc@gmail.com");
        buyerBidDetails.add(buyerBidDetail);
        return buyerBidDetails;
    }

}
