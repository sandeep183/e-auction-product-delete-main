package com.eauction.seller.deleteproduct.exception;

public class BidExistsException extends RuntimeException {
    public BidExistsException(String message) {
        super(message);
    }
}