package com.eauction.seller.deleteproduct.exception;

public class BidEndedException extends RuntimeException{
    public BidEndedException(String message) {
        super(message);
    }
}