package com.eauction.seller.deleteproduct;

import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.function.context.FunctionRegistration;
import org.springframework.cloud.function.context.FunctionType;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.GenericApplicationContext;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.eauction.seller.deleteproduct.dto.ProductDeleteInputDTO;
import com.eauction.seller.deleteproduct.entity.ProductDetail;
import com.eauction.seller.deleteproduct.service.DeleteProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@SpringBootApplication
@Log4j2
@RequiredArgsConstructor
public class DeleteProductApplication implements ApplicationContextInitializer<GenericApplicationContext> {

	private static final Logger log = LogManager.getLogger(DeleteProductApplication.class);
	@Autowired
	private  DeleteProductService deleteProductService;

	public static void main(String[] args) {
		SpringApplication.run(DeleteProductApplication.class, args);
	}

	@Bean
	public Function<APIGatewayProxyRequestEvent, ProductDetail> productDeletionFunction() {
		return input -> {
			log.debug("Received input {}", input);
			try {
				String queryParam = input.getQueryStringParameters().get("productId");
				log.debug("queryParam {}", queryParam);
				log.debug("input.getBody() {}", input.getBody());
				ProductDeleteInputDTO inputDto = null;
				if(queryParam.isEmpty()){
					ObjectMapper objectMapper = new JsonMapper();
					log.debug("input.getBody() {}", input.getBody());
					inputDto = objectMapper.readValue(input.getBody(), ProductDeleteInputDTO.class);
				}else{
					inputDto = new ProductDeleteInputDTO();
					inputDto.setProductId(queryParam);
				}
				//ProductDeleteInputDTO inputDto =  new ProductDeleteInputDTO();
				inputDto.setProductId(queryParam);
				return deleteProductService.deleteProductDetails(inputDto);
			} catch (Exception exception) {
				log.error("Exception occurred during lambda execution. {}", exception.getMessage());
				return new ProductDetail();
			}
		};
	}

	@Override
	public void initialize(GenericApplicationContext context) {
		context.registerBean("productDeletionFunction", FunctionRegistration.class,
				() -> new FunctionRegistration<>(productDeletionFunction())
						.type(FunctionType.from(ProductDeleteInputDTO.class).to(ProductDetail.class).getType()));
	}


}