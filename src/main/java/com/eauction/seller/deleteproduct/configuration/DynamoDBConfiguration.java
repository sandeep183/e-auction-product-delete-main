package com.eauction.seller.deleteproduct.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@Configuration
public class DynamoDBConfiguration {

    @Bean
    public DynamoDbEnhancedClient dynamoDbEnhancedClient(){
        return  DynamoDbEnhancedClient.builder()
                .dynamoDbClient(DynamoDbClient.builder()
                        .credentialsProvider(StaticCredentialsProvider.create(amazonAWSCredentials()))
                        .region(Region.EU_WEST_2)
                        .build())
                .build();
    }
    
    @Bean
  public AwsCredentials amazonAWSCredentials() {
      return AwsBasicCredentials.create("AKIA5LLXHX4MVJADPNP4", "7jEqEgCtm5RWAEWCyvnSLADcpKTHbMHlLqN+XKel");
  }

}