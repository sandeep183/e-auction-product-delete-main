package com.eauction.seller.deleteproduct.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;

@Configuration
public class SnsConfiguration {

    @Bean
    public SnsClient snsClient(){
        return  SnsClient.builder().region(Region.EU_WEST_2).build();
    }

}