package com.eauction.seller.deleteproduct.repository;

import static software.amazon.awssdk.enhanced.dynamodb.mapper.StaticAttributeTags.primaryPartitionKey;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.eauction.seller.deleteproduct.entity.ProductDetail;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;

@Repository
@RequiredArgsConstructor
@Log4j2
public class ProductDetailsRepository {
	private static final Logger log = LogManager.getLogger(ProductDetailsRepository.class);
	@Autowired
    private  DynamoDbEnhancedClient dynamoDbEnhancedClient;

    private static final TableSchema<ProductDetail> PRODUCT_DETAILS_TABLE_SCHEMA =
            TableSchema.builder(ProductDetail.class)
                    .newItemSupplier(ProductDetail::new)
                    .addAttribute(String.class, a -> a.name("productId")
                            .getter(ProductDetail::getProductId)
                            .setter(ProductDetail::setProductId)
                            .tags(primaryPartitionKey()))
                    .addAttribute(String.class, a -> a.name("productName")
                            .getter(ProductDetail::getProductName)
                            .setter(ProductDetail::setProductName))
                    .addAttribute(String.class, a -> a.name("shortDescription")
                            .getter(ProductDetail::getShortDescription)
                            .setter(ProductDetail::setShortDescription))
                    .addAttribute(String.class, a -> a.name("detailedDescription")
                            .getter(ProductDetail::getDetailedDescription)
                            .setter(ProductDetail::setDetailedDescription))
                    .addAttribute(String.class, a -> a.name("category")
                            .getter(ProductDetail::getCategory)
                            .setter(ProductDetail::setCategory))
                    .addAttribute(String.class, a -> a.name("startingPrice")
                            .getter(ProductDetail::getStartingPrice)
                            .setter(ProductDetail::setStartingPrice))
                    .addAttribute(String.class, a -> a.name("bidEndDate")
                            .getter(ProductDetail::getBidEndDate)
                            .setter(ProductDetail::setBidEndDate))
                    .addAttribute(String.class, a -> a.name("firstName")
                            .getter(ProductDetail::getFirstName)
                            .setter(ProductDetail::setFirstName))
                    .addAttribute(String.class, a -> a.name("lastName")
                            .getter(ProductDetail::getLastName)
                            .setter(ProductDetail::setLastName))
                    .addAttribute(String.class, a -> a.name("address")
                            .getter(ProductDetail::getAddress)
                            .setter(ProductDetail::setAddress))
                    .addAttribute(String.class, a -> a.name("city")
                            .getter(ProductDetail::getCity)
                            .setter(ProductDetail::setCity))
                    .addAttribute(String.class, a -> a.name("state")
                            .getter(ProductDetail::getState)
                            .setter(ProductDetail::setState))
                    .addAttribute(String.class, a -> a.name("pin")
                            .getter(ProductDetail::getPin)
                            .setter(ProductDetail::setPin))
                    .addAttribute(String.class, a -> a.name("phone")
                            .getter(ProductDetail::getPhone)
                            .setter(ProductDetail::setPhone))
                    .addAttribute(String.class, a -> a.name("email")
                            .getter(ProductDetail::getEmail)
                            .setter(ProductDetail::setEmail))
                    .build();


    public ProductDetail deleteProduct(String productId) {
        final DynamoDbTable<ProductDetail> productDetailsTable = dynamoDbEnhancedClient.table("product-details",
                PRODUCT_DETAILS_TABLE_SCHEMA);
        ProductDetail deletedProduct =  productDetailsTable.deleteItem(Key.builder().partitionValue(productId).build());
        log.debug("Deleted Product: {}",deletedProduct);
        return deletedProduct;
    }

    public ProductDetail fetchProductById(String productId) {
        final DynamoDbTable<ProductDetail> productDetailsTable = dynamoDbEnhancedClient.table("product-details",
                PRODUCT_DETAILS_TABLE_SCHEMA);
        return productDetailsTable.getItem(Key.builder().partitionValue(productId).build());
    }


}
