package com.eauction.seller.deleteproduct.dto;

import lombok.Data;

@Data
public class ProductDeleteInputDTO {
    private String productId;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
}
