package com.eauction.seller.deleteproduct.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.eauction.seller.deleteproduct.dto.ProductDeleteInputDTO;
import com.eauction.seller.deleteproduct.entity.ProductDetail;
import com.eauction.seller.deleteproduct.exception.BidEndedException;
import com.eauction.seller.deleteproduct.exception.BidExistsException;
import com.eauction.seller.deleteproduct.repository.BidDetailsRepository;
import com.eauction.seller.deleteproduct.repository.ProductDetailsRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;
import software.amazon.awssdk.utils.CollectionUtils;
import software.amazon.awssdk.utils.StringUtils;

@Service
@RequiredArgsConstructor
@Log4j2
public class DeleteProductService {

	private static final Logger log = LogManager.getLogger(DeleteProductService.class);
	@Autowired
    private  ProductDetailsRepository productDetailsRepository;
	@Autowired
    private  BidDetailsRepository bidDetailsRepository;
	@Autowired
    private  SnsClient snsClient;

    public ProductDetail deleteProductDetails(ProductDeleteInputDTO productDeleteInputDTO) {
        log.debug("Received input: {}", productDeleteInputDTO);
        if(Objects.nonNull(productDeleteInputDTO) && !StringUtils.isEmpty(productDeleteInputDTO.getProductId())) {
            validateBidAssociation(productDeleteInputDTO.getProductId());
            validateBidEndDateOfProduct(productDeleteInputDTO.getProductId());
            sendProductDeleteNotification(productDeleteInputDTO);
            return productDetailsRepository.deleteProduct(productDeleteInputDTO.getProductId());
        }
        return new ProductDetail();
    }

    private void validateBidAssociation(String productId) {
        if(!CollectionUtils.isNullOrEmpty(bidDetailsRepository.fetchBids(productId))) {
            throw new BidExistsException("Product cannot be deleted. One or more Bids associated with this product.");
        }
        log.debug("validateBidAssociation passed");
    }

    private void validateBidEndDateOfProduct(String productId) {
        ProductDetail productDetail = productDetailsRepository.fetchProductById(productId);
        if(Objects.nonNull(productDetail) && !StringUtils.isEmpty(productDetail.getBidEndDate())) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate bidDate = LocalDate.parse(productDetail.getBidEndDate(),formatter);
            if(bidDate.isBefore(LocalDate.now())) {
                throw new BidEndedException("Product cannot be deleted. Bid end date is completed");
            }
        }
        log.debug("validateBidEndDateOfProduct passed");
    }

    private void sendProductDeleteNotification(ProductDeleteInputDTO productDeleteInputDTO) {
        String message = "Product deletion initiated for product id: "+ productDeleteInputDTO.getProductId();
        snsClient =  SnsClient.builder()
        .region(Region.EU_WEST_2)
        .credentialsProvider(StaticCredentialsProvider.create(amazonAWSCredentialsNew()))
        .build();
        PublishResponse response = snsClient.publish(PublishRequest.builder()
                .topicArn("arn:aws:sns:eu-west-2:917762457369:product-delete-topic")
                .message(message).build());
        log.debug("SNS Publish Response: {}", response);
        snsClient.close();
    }
    
    @Bean
    private AwsCredentials amazonAWSCredentialsNew() {
      return AwsBasicCredentials.create("AKIA5LLXHX4MVJADPNP4", "7jEqEgCtm5RWAEWCyvnSLADcpKTHbMHlLqN+XKel");
  }


}